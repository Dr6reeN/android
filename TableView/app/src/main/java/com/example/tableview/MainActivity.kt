package com.example.tableview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val image: ImageView = findViewById(R.id.Image)
        val button: Button = findViewById(R.id.Button)
        button.setOnClickListener {
            when(Random.nextInt(1,3)){
                1 -> image.setImageDrawable(getDrawable(R.drawable.porsche1))
                2 -> image.setImageDrawable(getDrawable(R.drawable.porsche2))
                3 -> image.setImageDrawable(getDrawable(R.drawable.Pray))
            }
            val color = Color.argb(255,
                Random.nextInt(0,255),
                Random.nextInt(0,255),
                Random.nextInt(0,255))
            button.setBackgroundColor(color)
        }
    }
}